---
minimum_pre_commit_version: 2.10.1
repos:
  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v4.4.0
    hooks:
      - id: check-merge-conflict
        description: Check for files that contain merge conflict strings.
        language_version: python3
      - id: trailing-whitespace
        description: Trims trailing whitespace.
        args: [--markdown-linebreak-ext=md]
        language_version: python3
      - id: mixed-line-ending
        description: Replaces or checks mixed line ending.
        args: [--fix=lf]
        exclude: make.bat
        language_version: python3
      - id: fix-byte-order-marker
        description: Removes UTF-8 BOM if present, generally a Windows problem.
        language_version: python3
      - id: end-of-file-fixer
        description: Makes sure files end in a newline and only a newline.
        exclude: tests/fake_.*\.key
        language_version: python3
      - id: check-ast
        description: Simply check whether files parse as valid python.
        language_version: python3
      - id: check-yaml
      - id: check-json

  # ----- Formatting ---------------------------------------------------------------------------->
  - repo: https://github.com/myint/autoflake
    rev: v2.1.1
    hooks:
      - id: autoflake
        name: Remove unused variables and imports
        language: python
        args: ["--in-place", "--remove-unused-variables", "--expand-star-imports"]
        files: \.py$

  - repo: https://github.com/saltstack/pre-commit-remove-import-headers
    rev: 1.1.0
    hooks:
      - id: remove-import-headers

  - repo: https://github.com/asottile/pyupgrade
    rev: v3.4.0
    hooks:
      - id: pyupgrade
        name: Rewrite Code to be Py3.8+
        args: [--py38-plus]

  - repo: https://github.com/asottile/reorder_python_imports
    rev: v3.9.0
    hooks:
      - id: reorder-python-imports
        args: [--py38-plus]

  - repo: https://github.com/psf/black
    rev: 23.3.0
    hooks:
      - id: black
        args: []

  - repo: https://github.com/asottile/blacken-docs
    rev: 1.13.0
    hooks:
      - id: blacken-docs
        description: Run black on python code blocks in documentation files.
        files: ^(index|docs/.*|docs/_includes/.*)\.rst$
        additional_dependencies: [black==22.6.0]

  - repo: https://github.com/myint/rstcheck
    rev: 'v6.1.2'
    hooks:
      - id: rstcheck
        name: Check reST files using rstcheck
        args: [--report-level=warning]
        additional_dependencies: [sphinx]
  # <---- Formatting -----------------------------------------------------------------------------

  # ----- Testing Static Requirements ------------------------------------------------------------------------------------>
  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: '4.8'
    hooks:
      - id: pip-tools-compile
        alias: compile-3.8-test-requirements
        name: Py3.8 Test Requirements
        files: ^requirements/tests.in$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.8
          - --platform=linux
          - requirements/tests.in

      - id: pip-tools-compile
        alias: compile-3.9-test-requirements
        name: Py3.9 Test Requirements
        files: ^requirements/tests.in$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.9
          - --platform=linux
          - requirements/tests.in

      - id: pip-tools-compile
        alias: compile-3.10-test-requirements
        name: Py3.10 Test Requirements
        files: ^requirements/tests.in$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.10
          - --platform=linux
          - requirements/tests.in

      - id: pip-tools-compile
        alias: compile-3.11-test-requirements
        name: Py3.11 Test Requirements
        files: ^requirements/tests.in$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.11
          - --platform=linux
          - requirements/tests.in

  # <---- Testing Static Requirements -------------------------------------------------------------------------------------

  # ----- Docs -------------------------------------------------------------------------------------------------->

#  - repo: https://github.com/saltstack/mirrors-nox
#    rev: v2021.6.12
#    hooks:
#      - id: nox
#        alias: Generate Sphinx docs
#        description: Generate Sphinx docs, ensure they build
#        args:
#          - -e
#          - 'docs-html(clean=True)'
#          - --

  # <---- Docs ---------------------------------------------------------------------------------------------------

  # ----- Security -------------------------------------------------------------------------------------------------->
  - repo: https://github.com/PyCQA/bandit
    rev: "1.7.5"
    hooks:
      - id: bandit
        name: Run bandit against POP project
        args: [--silent, -lll]
        files: .*\.py
        exclude: >
            (?x)^(
                tests/.*
            )$

  # <---- Security ---------------------------------------------------------------------------------------------------

  # ----- Build Test -------------------------------------------------------------------------------------------------->
  - repo: https://github.com/miki725/pre-commit-twine-check
    rev: '0.1'
    hooks:
      - id: twine-check
  # <---- Build Test ---------------------------------------------------------------------------------------------------
