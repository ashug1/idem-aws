import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_rest_api(hub, ctx):
    # Create rest api
    rest_api_name = "idem-test-rest_api_-" + str(uuid.uuid4())
    tags = {"Name": rest_api_name}

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    comment_utils_kwargs = {
        "resource_type": "aws.apigateway.rest_api",
        "name": rest_api_name,
    }
    created_message = hub.tool.aws.comment_utils.create_comment(**comment_utils_kwargs)[
        0
    ]
    would_create_message = hub.tool.aws.comment_utils.would_create_comment(
        **comment_utils_kwargs
    )[0]
    deleted_message = hub.tool.aws.comment_utils.delete_comment(**comment_utils_kwargs)[
        0
    ]
    would_delete_message = hub.tool.aws.comment_utils.would_delete_comment(
        **comment_utils_kwargs
    )[0]
    already_absent_message = hub.tool.aws.comment_utils.already_absent_comment(
        **comment_utils_kwargs
    )[0]
    would_update_message = hub.tool.aws.comment_utils.would_update_comment(
        **comment_utils_kwargs
    )[0]
    update_message = hub.tool.aws.comment_utils.update_comment(**comment_utils_kwargs)[
        0
    ]

    # --test create
    ret = await hub.states.aws.apigateway.rest_api.present(
        test_ctx,
        name=rest_api_name,
        description="For testing idem plugin",
        disable_execute_api_endpoint=False,
        api_key_source="HEADER",
        endpoint_configuration={"types": ["REGIONAL"]},
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert would_create_message in ret["comment"]
    resource = ret["new_state"]
    assert rest_api_name == resource["name"]
    assert "For testing idem plugin" == resource["description"]
    assert tags == resource["tags"]
    assert "HEADER" == resource["api_key_source"]
    assert False == resource["disable_execute_api_endpoint"]
    assert "REGIONAL" in str(resource["endpoint_configuration"])

    # real create
    ret = await hub.states.aws.apigateway.rest_api.present(
        ctx,
        name=rest_api_name,
        description="For testing idem plugin",
        disable_execute_api_endpoint=False,
        api_key_source="HEADER",
        endpoint_configuration={"types": ["REGIONAL"]},
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert created_message in ret["comment"]
    resource = ret["new_state"]
    assert resource["name"] == rest_api_name
    assert "For testing idem plugin" == resource["description"]
    assert tags == resource["tags"]
    resource_id = resource["resource_id"]
    assert "HEADER" == resource["api_key_source"]
    assert False == resource["disable_execute_api_endpoint"]
    assert "REGIONAL" in str(resource["endpoint_configuration"])

    # Describe instance
    describe_ret = await hub.states.aws.apigateway.rest_api.describe(ctx)
    resource = dict(
        ChainMap(*describe_ret[rest_api_name].get("aws.apigateway.rest_api.present"))
    )
    assert resource_id == resource.get("resource_id")
    assert rest_api_name == resource.get("name")
    assert tags == resource["tags"]
    assert "For testing idem plugin" == resource["description"]
    assert "HEADER" == resource["api_key_source"]
    assert False == resource["disable_execute_api_endpoint"]
    assert "REGIONAL" in str(resource["endpoint_configuration"])
    assert "HEADER" == resource["api_key_source"]
    assert False == resource["disable_execute_api_endpoint"]
    assert "REGIONAL" in str(resource["endpoint_configuration"])

    # --test update, no changes
    ret = await hub.states.aws.apigateway.rest_api.present(
        test_ctx,
        name=rest_api_name,
        resource_id=resource_id,
        description="For testing idem plugin",
        disable_execute_api_endpoint=False,
        api_key_source="HEADER",
        endpoint_configuration={"types": ["REGIONAL"]},
        tags=tags,
    )
    assert "already exists" in ret["comment"][0]

    # --test update
    tags2 = {"Name": f"{rest_api_name}1"}
    ret = await hub.states.aws.apigateway.rest_api.present(
        test_ctx,
        name=rest_api_name,
        resource_id=resource_id,
        description="For testing idem plugin1",
        disable_execute_api_endpoint=False,
        api_key_source="HEADER",
        endpoint_configuration={"types": ["PRIVATE"]},
        tags=tags2,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert would_update_message in ret["comment"]
    resource = ret["new_state"]
    assert resource["name"] == rest_api_name
    assert "For testing idem plugin1" == resource["description"]
    assert tags2 == resource["tags"]
    resource_id = resource["resource_id"]
    assert "HEADER" == resource["api_key_source"]
    assert False == resource["disable_execute_api_endpoint"]
    assert "PRIVATE" in str(resource["endpoint_configuration"])

    # --test update, invalid endpoint configuration (can't change length of types)
    tags2 = {"Name": f"{rest_api_name}1"}
    ret = await hub.states.aws.apigateway.rest_api.present(
        test_ctx,
        name=rest_api_name,
        resource_id=resource_id,
        description="For testing idem plugin1",
        disable_execute_api_endpoint=False,
        api_key_source="HEADER",
        endpoint_configuration={"types": ["PRIVATE", "EDGE"]},
        tags=tags2,
    )
    assert not ret["result"]
    assert "AWS doesn't support changing the number of endpoint types" in str(
        ret["comment"]
    )
    if not hub.tool.utils.is_running_localstack(ctx):
        # localstack cant do a real update
        # real update
        ret = await hub.states.aws.apigateway.rest_api.present(
            ctx,
            name=rest_api_name,
            resource_id=resource_id,
            description="For testing idem plugin1",
            disable_execute_api_endpoint=False,
            api_key_source="HEADER",
            endpoint_configuration={"types": ["PRIVATE"]},
            tags=tags2,
        )
        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and ret.get("new_state")
        assert update_message in ret["comment"]
        resource = ret["new_state"]
        assert resource["name"] == rest_api_name
        assert "For testing idem plugin1" == resource["description"]
        assert tags2 == resource["tags"]
        resource_id = resource["resource_id"]
        assert "HEADER" == resource["api_key_source"]
        assert False == resource["disable_execute_api_endpoint"]
        assert "PRIVATE" in str(resource["endpoint_configuration"])

        # localstack cant update tags
        # --test update undo it
        ret = await hub.states.aws.apigateway.rest_api.present(
            test_ctx,
            name=rest_api_name,
            resource_id=resource_id,
            description="For testing idem plugin",
            disable_execute_api_endpoint=False,
            api_key_source="HEADER",
            endpoint_configuration={"types": ["REGIONAL"]},
            tags=tags,
        )
        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and ret.get("new_state")
        assert would_update_message in ret["comment"]
        resource = ret["new_state"]
        assert resource["name"] == rest_api_name
        assert "For testing idem plugin" == resource["description"]
        assert tags == resource["tags"]
        resource_id = resource["resource_id"]
        assert "HEADER" == resource["api_key_source"]
        assert False == resource["disable_execute_api_endpoint"]
        assert "REGIONAL" in str(resource["endpoint_configuration"])

        # real update tags undo it
        ret = await hub.states.aws.apigateway.rest_api.present(
            ctx,
            name=rest_api_name,
            resource_id=resource_id,
            description="For testing idem plugin",
            disable_execute_api_endpoint=False,
            api_key_source="HEADER",
            endpoint_configuration={"types": ["REGIONAL"]},
            tags=tags,
        )
        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and ret.get("new_state")
        assert update_message in ret["comment"]
        resource = ret["new_state"]
        assert resource["name"] == rest_api_name
        assert "For testing idem plugin" == resource["description"]
        assert tags == resource["tags"]
        resource_id = resource["resource_id"]
        assert "HEADER" == resource["api_key_source"]
        assert False == resource["disable_execute_api_endpoint"]
        assert "REGIONAL" in str(resource["endpoint_configuration"])

    # Delete instance with --test as true
    ret = await hub.states.aws.apigateway.rest_api.absent(
        test_ctx, name=rest_api_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert would_delete_message in ret["comment"]
    resource = ret.get("old_state")
    assert rest_api_name == resource.get("name")
    assert "For testing idem plugin" == resource["description"]
    assert "HEADER" == resource["api_key_source"]
    assert False == resource["disable_execute_api_endpoint"]

    # Delete instance
    ret = await hub.states.aws.apigateway.rest_api.absent(
        ctx, name=rest_api_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert deleted_message in ret["comment"]
    resource = ret.get("old_state")
    assert rest_api_name == resource.get("name")
    assert "For testing idem plugin" == resource["description"]
    assert "HEADER" == resource["api_key_source"]
    assert False == resource["disable_execute_api_endpoint"]

    # Delete instance without resource_id
    ret = await hub.states.aws.apigateway.rest_api.absent(ctx, name=rest_api_name)
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
    assert already_absent_message in ret["comment"]

    # should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.apigateway.rest_api.absent(
        ctx, name=rest_api_name, resource_id=resource_id
    )
    assert already_absent_message in ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
