import time

import pytest


@pytest.mark.asyncio
async def test_caller_identity(hub, ctx):
    elb_hosted_zone_id_name = "idem-test-elb-hosted-zone-id-" + str(int(time.time()))

    # Test elb-hosted-zone-id with region name
    search_ret = await hub.exec.aws.elb.elb_hosted_zone_id.get(
        ctx, name=elb_hosted_zone_id_name, region="eu-west-2"
    )
    assert search_ret["result"], search_ret["comment"]
    assert search_ret["ret"]
    resource = search_ret["ret"]
    assert elb_hosted_zone_id_name == resource["name"]
    assert resource["elb_hosted_zone_id"]
    assert resource["elb_hosted_zone_id"] == "ZHURV8PSTC4K8"

    # Test elb-hosted-zone-id without specifying region
    search_ret = await hub.exec.aws.elb.elb_hosted_zone_id.get(
        ctx, name=elb_hosted_zone_id_name
    )
    assert search_ret["result"], search_ret["comment"]
    assert search_ret["ret"]
    resource = search_ret["ret"]
    assert elb_hosted_zone_id_name == resource["name"]
    assert resource["elb_hosted_zone_id"]
    assert resource["elb_hosted_zone_id"] == "Z1H1FL5HABSF5"
