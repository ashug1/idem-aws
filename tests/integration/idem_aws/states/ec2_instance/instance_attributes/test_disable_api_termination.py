import copy

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_update_disable_api_termination(hub, ctx, aws_ec2_instance, __test):
    state = copy.copy(aws_ec2_instance)
    state["disable_api_termination"] = True

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert ret["changes"]["new"]["disable_api_termination"] is True
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_reset_disable_api_termination(hub, ctx, aws_ec2_instance, __test):
    state = copy.copy(aws_ec2_instance)
    state["disable_api_termination"] = False

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert ret["changes"]["new"]["disable_api_termination"] is False
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]
