def test_cloudtrail_utils(hub):
    # plan state should be same as update_ret state
    plan_state = {}
    update_ret = {
        "ret": {
            "s3_bucket_name": "test_bucket",
            "sns_topic_name": "test_sns_topic",
            "is_multi_region_trail": "true",
        }
    }
    result = hub.tool.aws.cloudtrail.conversion_utils.update_plan_state(
        plan_state, update_ret
    )
    assert result == update_ret["ret"]
