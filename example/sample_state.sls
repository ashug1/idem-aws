{%  for i in range(1) %}
idem_aws_demo_user_{{i}}:
  aws.iam.user.present
{%  endfor -%}

my_role:
  aws.iam.role.present:
    - assume_role_policy_document: "{}"

my_idem_vpc:
  aws.ec2.vpc.present:
    - cidr_block_association_set:
      - CidrBlock: 10.0.0.1/24
    - tags:
      - Key: idem_demo_key
        Value: my_vpc

my_idem_subnet:
  aws.ec2.subnet.present:
    - vpc_id: ${aws.ec2.vpc:my_idem_vpc:resource_id}
    - cidr_block: 10.0.0.0/28
    - tags:
      - Key: idem_demo_keys
        Value: my_subnet
