"""Tests for validating Rds Db Proxy Target Groups."""
import uuid
from collections import ChainMap

import pytest


PARAMETRIZE = {
    "argnames": "__test",
    "argvalues": [True, False],
    "ids": ["--test", "run"],
}

PARAMETER = {"name": "idem-test-resource-" + str(uuid.uuid4())}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, aws_rds_db_proxy, __test):
    r"""
    **Test function**
    """

    global PARAMETER
    ctx["test"] = __test
    # Create the resource
    ret = await hub.states.aws.rds.db_proxy_target_group.present(
        ctx,
        name=PARAMETER["name"],
        db_proxy_name=aws_rds_db_proxy.get("resource_id"),
        connection_pool_config={
            "MaxConnectionsPercent": 80,
            "MaxIdleConnectionsPercent": 10,
            "ConnectionBorrowTimeout": 60,
        },
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            f"Would create aws.rds.db_proxy_target_group '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Created aws.rds.db_proxy_target_group '{PARAMETER['name']}'"
            in ret["comment"]
        )

    PARAMETER["resource_id"] = resource["resource_id"]
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["name"] == resource.get("name")

    if not __test:
        # Now get the resource with exec
        ret = await hub.exec.aws.rds.db_proxy_target_group.get(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
        )
        assert ret
        assert ret["result"], ret["comment"]
        assert ret["ret"]
        resource = ret["ret"]
        assert PARAMETER["name"] == resource.get("name")
        assert "connection_pool_config" in resource
        connection_pool_config = resource["connection_pool_config"]
        assert 80 == connection_pool_config["MaxConnectionsPercent"]
        assert 10 == connection_pool_config["MaxIdleConnectionsPercent"]
        assert 60 == connection_pool_config["ConnectionBorrowTimeout"]

        # Now Update the resource
        ret = await hub.states.aws.rds.db_proxy_target_group.present(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
            db_proxy_name=aws_rds_db_proxy.get("resource_id"),
            connection_pool_config={
                "MaxConnectionsPercent": 85,
                "MaxIdleConnectionsPercent": 15,
                "ConnectionBorrowTimeout": 60,
            },
        )
        assert (
            f"Updated aws.rds.db_proxy_target_group '{PARAMETER['name']}'"
            in ret["comment"]
        )
        assert ret["result"], ret["comment"]

        assert ret.get("old_state") and ret.get("new_state")
        resource = ret["new_state"]
        assert "connection_pool_config" in resource
        connection_pool_config = resource["connection_pool_config"]
        assert 85 == connection_pool_config["MaxConnectionsPercent"]
        assert 15 == connection_pool_config["MaxIdleConnectionsPercent"]
        assert 60 == connection_pool_config["ConnectionBorrowTimeout"]


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    r"""
    **Test function**
    """

    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"
    ret = await hub.states.aws.rds.db_proxy_target_group.describe(
        ctx,
    )
    resource_id = PARAMETER["resource_id"]
    assert resource_id in ret
    assert "aws.rds.db_proxy_target_group.present" in ret[resource_id]
    described_resource = ret[resource_id].get("aws.rds.db_proxy_target_group.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert "connection_pool_config" in described_resource_map
    connection_pool_config = described_resource_map["connection_pool_config"]
    assert 85 == connection_pool_config["MaxConnectionsPercent"]
    assert 15 == connection_pool_config["MaxIdleConnectionsPercent"]
    assert 60 == connection_pool_config["ConnectionBorrowTimeout"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test):
    r"""
    **Test function**
    """

    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"
    ctx["test"] = __test
    # Delete the resource
    ret = await hub.states.aws.rds.db_proxy_target_group.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )

    assert f"cannot be deleted" in str(ret["comment"])

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
